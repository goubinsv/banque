package MonPackage;

public class Compte 
{
	private String nom ; 
	private int num ;
	protected int solde ;
	protected int decouvert ;
	
	
	public Compte()
	{
		nom = Saisie.lire_String("Quel est votre nom ? ") ;
		num = Saisie.lire_int("Quel est votre numéro de compte ? ") ;
		solde = Saisie.lire_int("Quel est le solde de votre compte ? ") ;
		decouvert = Saisie.lire_int("Quel est le decouvert autorisé de votre compte ? ") ;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public int getSolde() {
		return solde;
	}
	
	public void setSolde(int solde) {
		this.solde = solde;
	}

	public int getDecouvert() {
		return decouvert;
	}

	public void setDecouvert(int decouvert) {
		this.decouvert = decouvert;
	}
	
	
	public Compte (String nomBis, int numBis, int soldeBis, int decouvertBis) 
	{
		nom = nomBis ;
		num = numBis ;
		solde = soldeBis ;
		decouvert = decouvertBis ;
		
	}

	
	
	
	public void Consulte() 
	{
		System.out.println("Le solde de votre compte est de : " + solde);
	}
	
	public void Depot(int DepotArgent) 
	{
		solde = DepotArgent + solde ;
	}
	
	public void Retrait( int RetraitArgent ) 
	{
		solde = solde - RetraitArgent  ;
	}
	
	public void Affiche ()
	{
		System.out.println("Votre nom est  : " + nom);
		System.out.println("Votre numéro de compte est  : " + num);
		System.out.println("Le solde de votre compte est de : " + solde);
		System.out.println("Votre decouvert est de  : " + decouvert);
	}
	
	
}




