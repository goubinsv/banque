package MonPackage;

public class Compte_Epargne extends Compte 
{
	int Taux ;
	
	
	public Compte_Epargne(String nom, int num,int solde,int decouvert,int TauxBis )
	{
		super(nom,num,solde,decouvert); 
		Taux = TauxBis ;
	}
	
	public void Compte_EpargneConsulte()
	{
		super.Consulte(); 
	}
	
	public void MisAJours()
	{
		int Reponse ;
		Reponse = Saisie.lire_int("Taper 1 : Pour modifier le découvert. " +"\n"+ "Taper 2 : Pour faire un retrait. "+"\n"+ "Taper 3 : Pour deposer de l'argent. " ) ;
		
		switch (Reponse) 
		{
		case 1 :
			int DecouvertBis = Saisie.lire_int("Quel est votre nouveau decouvert ? " ) ;
			super.setDecouvert(DecouvertBis); 
			super.Affiche(); 
			break;
			

		case 2 : 
			int MonRetrait = Saisie.lire_int("Combien voulez vous retirer ? " ) ;
			super.Retrait( MonRetrait);
			super.Affiche(); 
			break;
			
		case 3 :
			int MonDepot = Saisie.lire_int("Combien voulez vous deposer ? " ) ;
			super.Depot(MonDepot);
			super.Affiche();
			break ;
		
		}
		
			
		
		
	}
}
