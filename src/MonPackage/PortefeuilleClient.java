package MonPackage;

import java.util.ArrayList;

public class PortefeuilleClient 
{
	private ArrayList<Compte> Collection ; 
	private int Compteur ;
	
	public PortefeuilleClient ()
	{
		 Collection = new ArrayList<Compte>()  ;
		 Compteur = 0 ;
		
	}
	public ArrayList<Compte> getCollection() {
		return Collection;
	}
	public void setCollection(ArrayList<Compte> collection) {
		Collection = collection;
	}
	public int getCompteur() {
		return Compteur;
	}
	public void setCompteur(int compteur) {
		Compteur = compteur;
	}
	public void AjoutNombre(int unNombre)
	{
		
		
		 for(int i = 0 ; i < unNombre ; i++) 
		 {
			Compte Uncompte = new Compte();
			Collection.add(Uncompte);
		 }
		
	}
	public void AfficheCompte() 
	{
		for(int i = 0 ; i < Collection.size();i++)
		{
			Collection.get(i).Affiche();
		}
	}
	
	
	
	
	public void TestListeVide()
	{
			 if(Collection.isEmpty()) 
			 {
				 System.out.println("Votre liste est vide");
			 }
			 else 
			 {
				 System.out.println("Votre liste n'est pas vide ");
			 }
		 
	}
	
	public void RechercheSolde() 
	{
		int UnnumCompte ;
		UnnumCompte  =  Saisie.lire_int("Quel est la reference du Compte ? ") ;
		for(int i = 0 ; i < Collection.size(); i++) 
		{
			if ( Collection.get(i).getNum()== UnnumCompte  )
			{
				Collection.get(i).Consulte();
			}
			
		}
	}
	
	public void CompteNegatif()
	{
		for(int i = 0 ; i < Collection.size(); i ++) 
		{
			if(Collection.get(i).getSolde() < 0 ) 
			{
				Collection.get(i).Affiche();
			}
		}
	}
	
	public void AjoutCompte() 
	{
		int UnNouveauCompte;
		UnNouveauCompte =  Saisie.lire_int("Combien de compte vouleza vous ajoutez ? ") ;
		for(int i = 0 ; i < UnNouveauCompte ; i++) 
		{
			Compte unCompte = new Compte();
			Collection.add(unCompte); 
		}
		 
	}
	
	public void ModifDecouvert(int UnNumero) 
	{
		int Changement ;
		for(int i = 0 ; i < Collection.size() ; i++) 
		{
			if(Collection.get(i).getNum()== UnNumero) 
			{
				Changement = Saisie.lire_int("Quel est le montant du nouveau decouvert ? ");
				Collection.get(i).setDecouvert(Changement);

			}
			
		} 
	}
	
	public void SuppCompte()
	{
		int Numero;
		Numero = Saisie.lire_int("Quel est le numero du compte que vous souhaiter supprimer");
		for (int i = 0 ; i< Collection.size(); i++ ) 
		{
			if(Collection.get(i).getNum()== Numero) 
			{
				Collection.remove(Collection.get(i));
				System.out.println("Le compte a bien été supprimer");
			}
		}
		AfficheCompte(); 
		
	}
	
	public void SuppCollection()
	{
			Collection.clear();
		
	}
	
	public void Transfert(Compte_Courant UnCompteCourant ,  Compte_Epargne UnCompteEpargne ) 
	{
		int MonTransfert = Saisie.lire_int("Quel est le montant que vous souhaiter transferer a votre compte courant ? ");
		UnCompteEpargne.Retrait(MonTransfert);
		UnCompteCourant.Depot(MonTransfert); 
		
		
		
		
	
		
		
	}
	
	
	
	
}

